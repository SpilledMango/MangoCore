public struct Queue<Element> {
    public var list: LinkedList<Element>

    // TODO Copy the list instead of using references.
    public init(list: LinkedList<Element>) {
        self.list = list
    }
    public init() {
        self = Queue(list: [])
    }

    public func enqueue(element: Element) {
        self.list.append(element)
    }
    public mutating func dequeue() -> Element? {
        if self.list.isEmpty {
            return nil
        }

        let item = self.list.first

        guard let next = self.list.next else {
            self.list.value = nil
            return item
        }

        self.list = next
        return item
    } 
}
