public final class LinkedList<Element> {
    public var value: Element?
    public var next: LinkedList<Element>? 

    public init(_ array: [Element]) {
        if array.isEmpty {
            return
        }
        self.value = array[0]

        var listItem = self
        for index in 1..<array.count {
            listItem.link(to: LinkedList<Element>(array[index]))
            listItem = listItem.next!
        }
    }

    public convenience init() {
        self.init([])
    }

    public convenience init(_ value: Element) {
        self.init([value])
    }

    public func insert(_ value: Element) {
        var item = self
        while (item.hasNext) {
            item = item.next!
        }
        item.link(to: LinkedList<Element>(value))
    }
    public var hasNext: Bool {
        return self.next != nil
    }
    public func link(to otherList: LinkedList<Element>) {
        self.next = otherList
    }

    public var count: Int {
        if self.value == nil {
            return 0
        }

        var listItem = self
        var count = 1

        while true {
            guard let next = listItem.next else {
                break;
            }
            listItem = next
            count += 1
        }
        return count
    }

    public var first: Element? {
        return self.value
    }
    public var lastItem: LinkedList<Element>? {
        if self.value == nil && self.next == nil {
            return nil
        }

        var list = self
        while let next = list.next {
            list = next
        }
        return list
    }
    public var last: Element? {
        return self.lastItem?.value
    }

    public var isEmpty: Bool {
        return self.count == 0
    }

    public func at(index: Int) -> LinkedList<Element> {
        guard index < self.count else {
            fatalError("LinkedList: Out of bounds (\(index) >= \(self.count))")
        }
        var list = self
        for _ in 0..<index {
            // list.next will always be non-nil because the index is already bounds-checked.
            list = list.next!
        }
        return list
    }

    public subscript (index: Int) -> Element {
        get {
            return self.at(index: index).value!
        }
        set {
            self.at(index: index).value = newValue
        }
    }
    public func append(_ element: Element) {
        guard let lastItem = self.lastItem else {
            self.value = element
            return
        }
        lastItem.link(to: LinkedList<Element>(element))
    }

}
extension LinkedList {
    public func forEach(_ body: (Element) throws -> Void) rethrows {
        if self.isEmpty {
            return
        }

        var list = self
        while let next = list.next {
            try body(list.value!)
            list = next
        }
    }
}
extension LinkedList: Equatable where Element: Equatable {
    public static func == <Element: Equatable>(lhs: LinkedList<Element>, rhs: LinkedList<Element>) -> Bool {
        if lhs.count != rhs.count {
            return false
        }
        var llist = lhs
        var rlist = rhs

        for _ in 0..<lhs.count - 1 {

            if (llist.value != rlist.value) {
                return false
            }
            llist = llist.next!
            rlist = rlist.next!
        }
        return llist.value == rlist.value
    }
}
extension LinkedList: ExpressibleByArrayLiteral {
    public typealias ArrayLiteralElement = Element

    public convenience init(arrayLiteral: ArrayLiteralElement...) {
        self.init(arrayLiteral)
    }
}
