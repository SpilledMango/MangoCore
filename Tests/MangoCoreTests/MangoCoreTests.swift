import XCTest
@testable import MangoCore

final class LinkedListTestCase: XCTestCase {
    private let array = [22, 125, 61, 451, 1337, 33]

    func initializationTest() {
        // Test if we can successfully initialize a linked list from a vector.
        let list = LinkedList<Int>(array)
        
        for index in 0..<array.count {
            XCTAssertEqual(array[index], list[index])
        }
    }

    func countTest() {
        // Test if the count variable works for the linked list.
        let list = LinkedList<Int>(array)

        XCTAssertEqual(array.count, list.count)
    }

    func equatableTest() {
        let list1 = LinkedList<Int>(array)
        let list2 = LinkedList<Int>(array)

        XCTAssertEqual(list1, list2)
    }

    func appendTest() {
        let list = LinkedList<Int>(array)

        list.forEach({
            item in
            print(item)
        })
        print("-")
        list.append(4)

        list.forEach({
            item in
            print(item)
        })
        print("-")

        let list2 = LinkedList<Int>()
        list2.append(12)
        list2.forEach {
            item in
            print(item)
        }
    }

    func setTest() {
        // Test if we can successfully set values in a linked list.
        let list: LinkedList<Int> = [234890, 123, 1653, 1635, 6324]
        let list2 = LinkedList<Int>(arrayLiteral: 1, 123, 1653, 1635, 6324)
        list[0] = 1
        XCTAssertEqual(list, list2)
    }


    static var allTests = [
        ("initializationTest", initializationTest),
        ("countTest", countTest),
        ("setTest", setTest),
        ("equatableTest", equatableTest),
        ("appendTest", appendTest),
    ]
}
final class QueueTestCase: XCTestCase {
    func testQueue() {
        var queue = Queue<Int>()
        
        for i in 0..<10 {
            queue.enqueue(element: i)
        }
        for _ in 0..<10 {
            print(queue.dequeue() ?? "nil")
        }
    }
    static var allTests = [
        ("testQueue", testQueue)
    ]
}
