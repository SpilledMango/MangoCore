import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(LinkedListTestCase.allTests),
        testCase(QueueTestCase.allTests),
    ]
}
#endif
