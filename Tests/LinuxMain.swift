import XCTest

import MangoCoreTests

var tests = [XCTestCaseEntry]()
tests += MangoCoreTests.allTests()
XCTMain(tests)